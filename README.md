# Curriculum-vitae

[Download the CV](https://gitlab.com/tiagoedgar/curriculum-vitae/-/raw/master/TiagoBarbosa.pdf?inline=false)


---
## DIY - Do It Yourself
This projects builds the CV using LaTeX. 

Feel free to clone it and build your own.

### How to use it

The LaTeX folder has a Makefile:  
1. Enter in the LaTeX folder: `cd LaTeX`
2. Install the required software (only once): `make instalTools`
3. Change/Edit the Latex files to match your own experience
4. Build the PDF file: `make`

> This will produce the PDF file inside the LaTeX folder and copy it to the root folder.
> Just repeat steps 3 and 4 while you build your own CV




